//var shuffleSequence = seq(sepWith("sep",rshuffle("item")));
var shuffleSequence = seq("item01","item02","item03");
var showProgressBar = true;
var progressBarText = "Fortschritt";


var defaults = [
	"AcceptabilityJudgment", {
		presentHorizontally: false,
		showNumbers: false,
		randomOrder: true
		},
	"Form",	{
		continueMessage:"Hier klicken um fortzufahren."
		},
	"Message",	{
		continueMessage:"Hier klicken um fortzufahren."
		},
	"Question",	{
			randomOrder: true	
	}
];


var items = [
	["setcounter", "__SetCounter__", {  }],
	
	[	"sep",
		"Separator",
		{transfer: 300,normalMessage:""}],
	
	[	'item01',
		"Form",
		{"html": {include: "Item01.html"}}],
 
	[	'item02',
		'AcceptabilityJudgment',
		{s: "Die drei Schauspieler Brad Pitt, Julia Roberts und Catherine Zeta-Jones gingen zu einer Premiere. Dabei saß er bereits im Filmsaal, während Brad Pitts Kollegen noch auf dem roten Teppich waren.",
		q:"Wer war im Filmsaal?",
		as: ["Brad Pitt", "George Clooney", "Julia Roberts", "Catherine Zeta-Jones", "jemand anderes (männlich)", "jemand anderes (weiblich)"],
			hasCorrect: 0
		}],
		
	[	"item03",
			"Message", {html: ["div",
            ["p", "Die drei Schauspieler Brad Pitt, Julia Roberts und Catherine Zeta-Jones gingen zu einer Premiere."],
            ["p", "Dabei saß er bereits im Filmsaal, während Brad Pitts Kollegen noch auf dem roten Teppich waren."]
               ]},
			"Question",
				{q: "Wer war im Filmsaal?",
				as: ["Brad Pitt", "George Clooney", "Matt Damon", "Julia Roberts", "Catherine Zeta-Jones","Angelina Jolie"],
				hasCorrect: 0
				}]
];